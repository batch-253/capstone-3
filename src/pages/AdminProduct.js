import { useEffect, useState } from "react";
import UpdateProductCard from "../components/UpdateProductCard";

export default function Products() {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetch(`http://localhost:4000/products/all`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        setProducts(
          data.map((product) => {
            return <UpdateProductCard key={product._id} product={product} />;
          })
        );
      });
  }, []);

  return <>{products}</>;
}
