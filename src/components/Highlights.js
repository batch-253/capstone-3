// //Bootstrap grid System components
// import { Row, Col, Card, Button } from "react-bootstrap";

// export default function Highlights() {
//   return (
//     <Row className="mt-3 mb-3">
//       <Col xs={12} md={4}>
//         <Card className="cardHighlight p-3">
//           <Card.Body>
//             <Card.Title>
//               <h2>BUY ONE GET ONE</h2>
//             </Card.Title>
//             <Card.Text>
//               Pariatur adipisicing aute do amet dolore cupidatat. Eu labore
//               aliqua eiusmod commodo occaecat mollit ullamco labore minim. Minim
//               irure fugiat anim ea sint consequat fugiat laboris id. Lorem elit
//               irure mollit officia incididunt ea ullamco laboris excepteur amet.
//               Cillum pariatur consequat adipisicing aute ex.
//             </Card.Text>
//           </Card.Body>
//         </Card>
//       </Col>
//       <Col xs={12} md={4}>
//         <Card className="cardHighlight p-3">
//           <Card.Body>
//             <Card.Title>
//               <h2>BUY ONE GET ONE</h2>
//             </Card.Title>
//             <Card.Text>
//               Pariatur adipisicing aute do amet dolore cupidatat. Eu labore
//               aliqua eiusmod commodo occaecat mollit ullamco labore minim. Minim
//               irure fugiat anim ea sint consequat fugiat laboris id. Lorem elit
//               irure mollit officia incididunt ea ullamco laboris excepteur amet.
//               Cillum pariatur consequat adipisicing aute ex.
//             </Card.Text>
//           </Card.Body>
//         </Card>
//       </Col>
//       <Col xs={12} md={4}>
//         <Card className="cardHighlight p-3">
//           <Card.Body>
//             <Card.Title>
//               <h2>BUY ONE GET ONE</h2>
//             </Card.Title>
//             <Card.Text>
//               Pariatur adipisicing aute do amet dolore cupidatat. Eu labore
//               aliqua eiusmod commodo occaecat mollit ullamco labore minim. Minim
//               irure fugiat anim ea sint consequat fugiat laboris id. Lorem elit
//               irure mollit officia incididunt ea ullamco laboris excepteur amet.
//               Cillum pariatur consequat adipisicing aute ex.
//             </Card.Text>
//           </Card.Body>
//         </Card>
//       </Col>
//     </Row>
//   );
// }

import { Row, Col, Card, Button } from "react-bootstrap";
import { NavLink } from "react-router-dom";

export default function Highlights() {
  return (
    <div>
      <Row className="mt-3 mb-3">
        <Col xs={12} md={4}>
          <Card className="cardHighlight bg-primary text-white p-3">
            <Card.Body>
              <Card.Title>
                <h2 className="text-white">Buy One, Get One</h2>
              </Card.Title>
              <Card.Text>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat.
              </Card.Text>
              <NavLink to="/products">
                <Button className="mt-3" variant="light">
                  Show Now
                </Button>
              </NavLink>
            </Card.Body>
          </Card>
        </Col>
        <Col xs={12} md={4}>
          <Card className="cardHighlight bg-warning text-white p-3">
            <Card.Body>
              <Card.Title>
                <h2 className="text-white">1% Off</h2>
              </Card.Title>
              <Card.Text>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat.
              </Card.Text>

              <NavLink to="/products">
                <Button className="mt-3" variant="light">
                  Show Now
                </Button>
              </NavLink>
            </Card.Body>
          </Card>
        </Col>
        <Col xs={12} md={4}>
          <Card className="cardHighlight bg-success text-white p-3">
            <Card.Body>
              <Card.Title>
                <h2 className="text-white">Free Shipping All Over The World</h2>
              </Card.Title>
              <Card.Text>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat.
              </Card.Text>
              <NavLink to="/products">
                <Button className="mt-3" variant="light">
                  Show Now
                </Button>
              </NavLink>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </div>
  );
}
