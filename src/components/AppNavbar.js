// import { useContext } from "react";

// import { Container, Navbar, Nav } from "react-bootstrap";
// import { Link, NavLink } from "react-router-dom";

// import UserContext from "../UserContext";

// export default function AppNavbar() {
//   const { user } = useContext(UserContext);

//   return (
//     <Navbar bg="light" expand="lg">
//       <Container fluid>
//         <Navbar.Brand as={Link} to="/">
//           Mekoy Ecommerce
//         </Navbar.Brand>
//         <Navbar.Toggle aria-controls="basic-navbar-nav" />
//         <Navbar.Collapse id="basic-navbar-nav">
//           <Nav className="ms-auto">
//             <Nav.Link as={NavLink} to="/">
//               Home
//             </Nav.Link>

//             <Nav.Link as={NavLink} to="/products">
//               Products
//             </Nav.Link>

//             {user.id !== null ? (
//               <Nav.Link as={NavLink} to="/logout">
//                 Logout
//               </Nav.Link>
//             ) : (
//               <>
//                 <Nav.Link as={NavLink} to="/login">
//                   Login
//                 </Nav.Link>
//                 <Nav.Link as={NavLink} to="/register">
//                   Register
//                 </Nav.Link>
//               </>
//             )}
//           </Nav>
//         </Navbar.Collapse>
//       </Container>
//     </Navbar>
//   );
// }

import { useContext } from "react";
import { Container, Navbar, Nav } from "react-bootstrap";
import { Link, NavLink } from "react-router-dom";
import UserContext from "../UserContext";

export default function AppNavbar() {
  const { user } = useContext(UserContext);

  return (
    <Navbar className="bg-dark mb-5">
      <Container fluid id="nav">
        <Navbar.Brand id="nav-link" as={Link} to="/">
          Mekoy Ecommerce
        </Navbar.Brand>

        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ms-auto">
            {!user.isAdmin && (
              <Nav.Link id="nav-link" as={NavLink} to="/">
                Home
              </Nav.Link>
            )}

            {!user.isAdmin && (
              <Nav.Link id="nav-link" as={NavLink} to="/products">
                Products
              </Nav.Link>
            )}

            {user.isAdmin && (
              <Nav.Link id="nav-link" as={NavLink} to="/products/all">
                All Products
              </Nav.Link>
            )}

            {user.id !== null && !user.isAdmin && (
              <Nav.Link id="nav-link" as={NavLink} to="/register">
                Register
              </Nav.Link>
            )}

            {user.isAdmin && (
              <Nav.Link id="nav-link" as={NavLink} to="/adminDashboard">
                Add Product
              </Nav.Link>
            )}

            {user.id !== null ? (
              <Nav.Link id="nav-link" as={NavLink} to="/logout">
                Logout
              </Nav.Link>
            ) : (
              <>
                <Nav.Link id="nav-link" as={NavLink} to="/login">
                  Login
                </Nav.Link>
                <Nav.Link id="nav-link" as={NavLink} to="/register">
                  Register
                </Nav.Link>
              </>
            )}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
